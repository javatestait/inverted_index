package asanteit.javatest;

import java.util.List;

/**
 * main de la aplicacion
 *@author fetchepare
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        InvertedIndex indice = new InvertedIndex();
        
        String[] data = new String[]{
        	 "A brilliant, festive study of JS Bach uses literature and painting to illuminate his 'dance-impregnated' music, writes Peter Conrad",
        	 "Fatima Bhutto on Malala Yousafzai's fearless and still-controversial memoir",
        	 "Grisham's sequel to A Time to Kill is a solid courtroom drama about racial prejudice marred by a flawless white hero, writes John O'Connell",
        	 "This strange repackaging of bits and pieces does the Man Booker winner no favours, says Sam Leith",
        	 "Another book with music related content"
        	    };
        /**
         * Carga el �ndice
         */
        indice.addToIndex(data);
        
        /**
         * query b�squeda de 1 o mas palabras
         */
        String[] search = new String[]{"music"};
        List<String> result = indice.get(search);
        
        
        /**
         * Muestra la cantidad de resultados por consola
         */
        System.out.println("Cantidad de oraciones que contienen la palabra buscada: "+result.size());
        
        /**
         * Muestra las oraciones que componen el resultado
         */
        int i = 0;
        while (i<result.size()){
        	System.out.println("'"+result.get(i)+"'");
        	i = i+1;
        }
        
    }
}
