package asanteit.javatest;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
* Clase que contiene la funcionalidad del �ndice invertido.
* Se maneja con 2 hashtables, una contiene las oraciones con un
* integer como clave y la oraci�n como valor
* y la otra es el indice invertido con las palabras como clave
* y como valor un arraylist que contiene en que oracion se encuentra
* la palabra clave.
* @author fetchepare
*
**/

public class InvertedIndex {
	
	private Hashtable<Integer,String> oraciones;
	private Hashtable<String,ArrayList<Integer>> indice;
	
	private int cantLineas;
	
	private static int INICIO=0;
	
	private Parser parser;
	
	/**
	 * constructor
	 */
	public InvertedIndex(){
		oraciones = new Hashtable<Integer,String>();
		indice = new Hashtable<String,ArrayList<Integer>>();
		cantLineas = INICIO;
		parser = new Parser();
	}

	/**
	 * Metodo publico para cargar las oraciones al �ndice
	 * @param String[] recibe un arreglo de Strings
	 * @return void
	 */
	public void addToIndex(String[] oracion){
		int i = 0;
		while (i< oracion.length){
			String[] palabras = parser.parsear(oracion[i]);
			this.add(palabras);
			oraciones.put(cantLineas, oracion[i]);
			cantLineas = cantLineas + 1;
			i = i + 1;
		}
	}
	
	/**
	 * M�todo privado de la clase que se encarga de 
	 * cargar el �ndice invertido
	 * Evita agregar palabras repetidas
	 * @param String[] recibe un arreglo de Strings
	 * @return void
	 * 
	 */
	
	private void add(String[] palabras){
		int i=0;
		while(i < palabras.length){
			String palabra = palabras[i];
			if (indice.containsKey(palabra)){
				List<Integer> cant = indice.get(palabra);		
				if (cantLineas != cant.get(cant.size()-1)){
					cant.add(cantLineas);
				}
			}
			else {
					ArrayList<Integer> enOracion = new ArrayList<Integer>();
					enOracion.add(cantLineas);
					indice.put(palabra,enOracion);
			}
			i = i + 1;
		}		
	}
	
	
	/**
	 * M�todo que se encarga de la b�squeda en el �ndice
	 * @param String recibe la palabra a buscar
	 * @return List<String> devuelve la lista de oraciones que contienen la
	 * palabra o las palabras buscadas
	 */
	
	public List<String> get(String[] palabrasBuscadas){
		int i=0;
		List<String> result = new ArrayList<String>();
		List<Integer> enOraciones = new ArrayList<Integer>();
		while(i < palabrasBuscadas.length){			
			if (indice.containsKey(palabrasBuscadas[i])){
				List<Integer> aux = indice.get(palabrasBuscadas[i]);				
				if (enOraciones.isEmpty()){
					enOraciones = aux;
				}
				else enOraciones.retainAll(aux);
			}
			i = i + 1;
		}
		int j = 0;
		if (enOraciones.isEmpty()){
			System.out.println("No se encontr� ning�n resultado.");
		} else {
				while (j < enOraciones.size()){
					result.add(oraciones.get(enOraciones.get(j)));
					j = j + 1;
				}
		}
		return result;
	}
}
