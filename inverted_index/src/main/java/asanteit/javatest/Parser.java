package asanteit.javatest;


/**
* Esta clase se encarga de parsear las oraciones
* contiene una lista de tokens que ayudan a identificar 
* las palabras.
* @author fetchepare
*
**/

public class Parser {

	private String tokens;
	
	public Parser(){
		tokens = "[ .,;?!��\'\"\\[\\]]+";
		
	}
	
	/**
	 * @param String recibe la oracion entera
	 * @return String[] devuelve un arreglo de Strings
	 * 			el cual cada posicion contiene una palabra
	 **/
	
	public String[] parsear(String oracion){
		String[] palabras = oracion.split(tokens);
		return palabras;
	}
}
